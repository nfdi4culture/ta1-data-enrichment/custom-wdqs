# NFDI4Culture Data Enrichment Query Service

#### Summary

This repository contains a fork of the [RHIZOME Artbase Query Service](https://github.com/rhizomedotorg/artbase-query-gui), itself a fork of the [Wikidata Query GUI](https://github.com/wikimedia/wikidata-query-gui). Configuration has been modified to update some stylistic elements as well as pointing the instance to the NFDI4Culture Data Enrichment triplestore.

#### Configuration

There are a few locations where configuration changes have occured.

[custom-config.json](custom-config.json) can be updated to include SPARQL endpoint, API address and some general branding.


```
{
    "api": {
        "sparql": {
            "uri": # Wikibase blazegraph SPARQL endpoint (should end in bigdata/namespace/wdq/sparql)
        },
        "wikibase": {
            "uri": # Wikibase API address (should end in /w/api.php)
        },
        "urlShortener": "tinyurl",
        "query-builder": {
            "server": "https://query-builder-test.toolforge.org/"
        }
    },
    "brand": {
        "title": # Query service name
        "logo": # Location of full logo
        "favicon": # Location of favicon logo
    },
    "location": {
        "root": "./",
        "index": "./"
    },
    "prefixes": {}
}
```

[custom-style.less](custom-style.less) has all applications of RHIZOME font customisation removed. 

[RdfNamespaces.js](wikibase/queryService/RdfNamespaces.js) has been updated so that all prefixes refer to NFDI4Culture Data Enrichment addresses. Prefixes themselves can also be updated (in this case from RHIZOME "r" to "tib").

[index.html](index.html) updated with minor cosmetic changes to the page header.

#### Prefixes

This image offers the following custom prefixes (which can be further configured in the [RdfNamespaces.js](wikibase/queryService/RdfNamespaces.js) file).

```yml
tib: 'https://mvp.tibwiki.io/entity/'
tibt: 'https://mvp.tibwiki.io/prop/direct/'
tibtn: 'https://mvp.tibwiki.io/prop/direct-normalized/'
tibs: 'https://mvp.tibwiki.io/entity/statement/'
tibp: 'https://mvp.tibwiki.io/prop/'
tibref: 'https://mvp.tibwiki.io/reference/'
tibv: 'https://mvp.tibwiki.io/value/'
tibps: 'https://mvp.tibwiki.io/prop/statement/'
tibpsv: 'https://mvp.tibwiki.io/prop/statement/value/'
tibpsn: 'https://mvp.tibwiki.io/prop/statement/value-normalized/'
tibpq: 'https://mvp.tibwiki.io/prop/qualifier/'
tibpqv: 'https://mvp.tibwiki.io/prop/qualifier/value/'
tibpqn: 'https://mvp.tibwiki.io/prop/qualifier/value-normalized/'
tibpr: 'https://mvp.tibwiki.io/prop/reference/'
tibprv: 'https://mvp.tibwiki.io/prop/reference/value/'
tibprn: 'https://mvp.tibwiki.io/prop/reference/value-normalized/'
tibno: 'https://mvp.tibwiki.io/prop/novalue/'
tibdata: 'https://mvp.tibwiki.io/wiki/Special:EntityData/'
```
	
#### Deployment

The new image can be build by navigating to the directory and executing
> docker build -t mvp-query:0.1 .

After the build process, the new image should be visible under `docker images`.

To deploy as part of `docker-compose.yml`, simply replace referenced image for wdqs-frontend, as so:
```yml
  wdqs-frontend:
    build: 
      context: ${WDQS_FRONTEND_CONTAINER}
      args:
        - WIKIBASE_SCHEME:
        - WIKIBASE_ADDRESS:
        - WIKIBASE_CONCEPT:
        - CUSTOM_CONFIG_QUERY:
``` 



~

→ [ArtBase README file for this repository](README_rhizome.md)     
→ [Original README file for this repository](README_wmde.md)   